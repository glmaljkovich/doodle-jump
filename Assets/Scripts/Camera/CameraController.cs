﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject player;

	private Vector3 offset;
	private float lastPlayerY;

	// Use this for initialization
	void Start () {
		offset = transform.position - player.transform.position;
		setLastPlayerY ();
	}
	
	// Update is called once per frame
	void Update () {
		if (playerGoingUp ()) {
			Vector3 reposition = transform.position;
			reposition.y = player.transform.position.y + offset.y;
			transform.position = reposition;
			setLastPlayerY ();
		}
	}

	private bool playerGoingUp (){
		return player.transform.position.y - lastPlayerY > 0;
	}

	private void setLastPlayerY(){
		lastPlayerY = player.transform.position.y;
	}
}
