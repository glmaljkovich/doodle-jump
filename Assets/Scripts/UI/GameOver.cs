﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
	public GameObject panel;
	public GameObject scoreObject;
	private Score score;
	public Text scoreText;
	public Player player;
	// Use this for initialization
	void Start () {
		score = scoreObject.GetComponent<Score> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (player.getIsDead()){
			panel.SetActive(true);
			scoreText.text = "Score: " + score.getScore();
		}
	}

	public void restartLevel(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
