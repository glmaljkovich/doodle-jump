﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
	public Text scoreText;
	private int score = 0;

	void Start ()
	{
		updateScoreText();
	}

	public void AddScore (int moreScore){
		this.score += moreScore;
		updateScoreText ();
	}

	private void updateScoreText(){
		scoreText.text = "Score: " + score;
	}

	public int getScore(){
		return score;
	}
}
