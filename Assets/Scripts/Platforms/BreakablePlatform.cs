﻿using UnityEngine;
using System.Collections;

public class BreakablePlatform : Platform
{
	Animator anim;

	// Use this for initialization
	void Awake ()
	{
		anim = GetComponent<Animator> ();
	}

	public override void doCustomBehavior(){
		anim.SetBool ("Break", true);
		print ("Override");
	}

}

