﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{
	[Range(1,2000)]
	public int points = 100;
	private GameObject scoreObject;
	protected Score score;
	protected bool firstHit;

	void Start ()
	{
		firstHit = true;
		scoreObject = GameObject.FindGameObjectWithTag ("Score");
		score = scoreObject.GetComponent<Score> ();
	}

	public void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == "Player" && firstHit) {
			addPoints (collision);
			this.doCustomBehavior ();
		}
	}

	protected void addPoints(Collision2D collision){
		score.AddScore (points);
		firstHit = false;
	}

	public virtual void doCustomBehavior(){
		print ("Virtual");
	}
}

