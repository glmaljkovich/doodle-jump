﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovementInY : MonoBehaviour {

	[Range(1, 100)]
	public float jumpSpeed;
	public bool grounded = false;
	public bool jumpBoost = false;
	private static int PLAYER_LAYER = 8;
	private static int JUMPING_PLAYER_LAYER = 10;

	private float speed;
	private Animator anim;
	private bool isDead = false;
	private Rigidbody2D rb;


	void Start () {
		speed = jumpSpeed;
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	void Update () {
		if (!isDead) {
			checkForJump ();
			checkForFall ();
		}
	}		

	private void checkForJump(){
		if (grounded && isNotGoingUp()) {
			changeSpeed ();
			rb.velocity = Vector2.up * speed;
			anim.SetTrigger ("Jump");
			gameObject.layer = JUMPING_PLAYER_LAYER;
		}
	}

	private void checkForFall (){
		if(isFalling()){
			gameObject.layer = PLAYER_LAYER;
		}
	}

	private void changeSpeed() {
		float factor = jumpBoost ? 1.5f : 1;
		speed = jumpSpeed * factor;
	}

	public void OnCollisionEnter2D (Collision2D collision) {
		switch (collision.gameObject.tag) {
		case "Ground":
			grounded = true;
			break;
		case "Spring":
			grounded = true;
			jumpBoost = true;
			break;
		default:
			break;
		}
	}

	private bool isNotGoingUp(){
		return rb.velocity.y <= 0;
	}

	private bool isFalling(){
		return rb.velocity.y < 0;
	}

	public void OnCollisionExit2D (Collision2D collision) {
		switch (collision.gameObject.tag) {
		case "Ground":
			grounded = false;
			break;
		case "Spring":
			grounded = false;
			jumpBoost = false;
			break;
		default:
			break;
		}
	}

}
