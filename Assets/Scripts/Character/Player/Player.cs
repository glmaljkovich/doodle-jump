﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	private bool isDead;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Die() {
		PlayerMovementInX movement = gameObject.GetComponent<PlayerMovementInX> ();
		movement.isDead = true; 
		this.isDead = true;
		movement.enabled = false;
		gameObject.GetComponent<Collider2D> ().enabled = false;
	}

	public bool getIsDead(){
		return this.isDead;
	}
}

