﻿using UnityEngine;
using System.Collections;

public class PlayerMovementInX : MonoBehaviour {

	[Range(1, 50)]
	public float walkSpeed;

	public bool isDead = false;
	private bool isRight;
	private Animator anim;

	void Start () {
		isRight = true;
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate () {
		if (!isDead) {
			move ();
			flip ();
		}
	}
		
	private void move(){
		var movement = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
		if (shouldMove())
			movePlayer (movement, walkSpeed);
	}

	private void movePlayer(Vector3 movement, float speed) {
		transform.position += movement * speed * Time.deltaTime;
	}

	private bool shouldMove(){
		return Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.LeftArrow);
	}

	private void flip(){
		float horizontal = Input.GetAxis ("Horizontal");
		if (horizontal > 0 && !isRight || horizontal < 0 && isRight) {
			isRight = !isRight;
			Vector3 scale = transform.localScale;
			scale.x *= -1;
			transform.localScale = scale;
		}
	}
		

}
